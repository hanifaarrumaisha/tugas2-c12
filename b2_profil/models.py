from django.db import models


class Company(models.Model):
    id = models.CharField(max_length=400, primary_key=True, default='not-set')
    name = models.CharField(max_length=400, null=True, default='not-set')
    email = models.CharField(max_length=100, default='not-set', null=True)
    com_type = models.CharField(max_length=100, default='not-set', null=True)
    industri = models.CharField(max_length=100, default='not-set', null=True)
    website = models.URLField(null=True)
    logo_url = models.CharField(max_length=400, default='not-set', null=True)
    size = models.CharField(max_length=100, default='not-set', null=True)
    desc = models.CharField(max_length=500, default='not-set', null=True)
    founded_year = models.CharField(max_length=100, default='not-set', null=True)
    followers = models.CharField(max_length=100, default='not-set', null=True)
    city = models.CharField(max_length=100, default='not-set', null=True)
# Create your models here.

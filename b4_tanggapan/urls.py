from django.conf.urls import url
from .views import add_reply, submit_reply

urlpatterns = [
    url(r'^add-reply/(?P<id>\d)', add_reply, name='add-reply'),
    url(r'^tanggapan/(?P<id>\d)', submit_reply, name='submit-reply')
]
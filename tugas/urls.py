"""tugas URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/dev/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  url(r'^$', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  url(r'^$', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.conf.urls import url, include
    2. Add a URL to urlpatterns:  url(r'^blog/', include('blog.urls'))
"""
from django.conf.urls import url, include
from django.contrib import admin
from django.views.generic.base import RedirectView
import b2_profil.urls as profil
import b1_utama.urls as login
import b3_forum.urls as forum
import b4_tanggapan.urls as tanggapan

urlpatterns = [
    url(r'^admin/', admin.site.urls),
    url(r'^$', RedirectView.as_view(url='/login/', permanent=True), name='r-login'),
    url(r'^login/', include(login, namespace='login')),
	url(r'^forum/', include(forum, namespace='forum')),
    url(r'^profil/', include(profil, namespace='profil')),
    url(r'^tanggapan/', include(tanggapan, namespace='tanggapan'))
]
